﻿
#region Imports

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.ServiceModel;
using System.ServiceProcess;
using System.Timers;

#endregion Imports

namespace ZLayer.SII.Service.Bot
{

    public partial class Process : ServiceBase
    {

        #region Properties

        private static Int32 iIntervalOneHour = iIntervalOneMinute * 60;
        private static Int32 iIntervalOneMinute = 1000 * 60;

        private static Timer tmrProcess = null;

        #endregion Properties


        #region Builders

        public Process()
        {
            InitializeComponent();
        }

        #endregion Builders


        #region Events

        #region Process

        private void tmrProcess_Tick(Object sender, ElapsedEventArgs e)
        {
            String[] lstProcessFiles = null;
            Portal.ServiceSoapClient svcSiiPortal = null;
            try
            {
                lstProcessFiles = GetDirectoryContent(ConfigurationManager.AppSettings["Directories:SendedInvoices"]
                    , Convert.ToInt32(ConfigurationManager.AppSettings["Files:FilterDays"]));
                if (lstProcessFiles != null && lstProcessFiles.Length > 0)
                {
                    svcSiiPortal = new Portal.ServiceSoapClient();
                    svcSiiPortal.Endpoint.Address = new EndpointAddress(ConfigurationManager.AppSettings["Paths:SiiPortal"]);
                    foreach (String itmFile in lstProcessFiles)
                    {
                        if (File.Exists(itmFile))
                        {
                            svcSiiPortal.EmitidasExcel(File.ReadAllBytes(itmFile), Path.GetFileName(itmFile));
                        }
                    }
                }
            }
            catch (Exception ectProcess)
            {
                SendMail(ConfigurationManager.AppSettings["Emails:Jose"], "Error: SII.Bot tmrProcess_Tick"
                    , (ectProcess.ToString()), true);
            }
        }

        #endregion Process


        #region Service

        protected override void OnStart(String[] prmArguments)
        {
            try
            {
                SendMail(ConfigurationManager.AppSettings["Emails:Jose"], "SII.Bot iniciado"
                    , "Fecha inicio: " + DateTime.Now.ToString(), true);
                tmrProcess = new Timer();
                tmrProcess.Interval = iIntervalOneHour;
                tmrProcess.Elapsed += new ElapsedEventHandler(tmrProcess_Tick);
                tmrProcess.Enabled = true;
            }
            catch (Exception)
            {
            }
        }

        protected override void OnStop()
        {
            try
            {
                tmrProcess.Enabled = false;
                SendMail(ConfigurationManager.AppSettings["Emails:Jose"], "SII.Bot parado"
                    , "Fecha paro: " + DateTime.Now.ToString(), true);
            }
            catch (Exception)
            {
            }
        }

        #endregion Service

        #endregion Events


        #region Methods

        #region Private

        private static String[] GetDirectoryContent(String prmDirectoryPath, Int32 prmDaysBefore)
        {
            FileInfo[] lstFiles = null;
            DateTime? dFilterDate = null;
            List<String> lstResult = null;
            try
            {
                if (!String.IsNullOrEmpty(prmDirectoryPath))
                {
                    lstFiles = GetDirectoryContent(prmDirectoryPath);
                    if (lstFiles != null && lstFiles.Length > 0)
                    {
                        dFilterDate = DateTime.Today.AddDays(-prmDaysBefore);
                        lstResult = new List<String>();
                        foreach (FileInfo itmFile in lstFiles)
                        {
                            if (itmFile.CreationTime >= dFilterDate)
                            {
                                lstResult.Add(itmFile.FullName);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                lstResult = null;
            }
            return lstResult.ToArray();
        }

        private static FileInfo[] GetDirectoryContent(String prmDirectoryPath)
        {
            DirectoryInfo itmDirectory = null;
            FileInfo[] lstFiles = null;
            try
            {
                if (!String.IsNullOrEmpty(prmDirectoryPath))
                {
                    itmDirectory = new DirectoryInfo(prmDirectoryPath);
                    lstFiles = itmDirectory.GetFiles();
                }
            }
            catch (Exception)
            {
                lstFiles = null;
            }
            return lstFiles;
        }

        private static Int32 SendMail(String prmReceiverEmail, String prmSubject, String prmBody, Boolean prmHasHtml)
        {
            Int32 iResult = 0;
            try
            {
                string Host = "mail.zertifika.com";
                string Origen = "info@zertifika.com";
                string Pass = "r3dM#z41#z41#z41";
                string User = "info@zertifika.com";
                MailMessage email = new MailMessage();
                email.To.Add(new MailAddress(prmReceiverEmail));
                email.From = new MailAddress(Origen);
                email.Subject = prmSubject;
                email.Body = prmBody;
                email.IsBodyHtml = prmHasHtml;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = Host;
                smtp.Credentials = new NetworkCredential(User, Pass);
                smtp.Send(email);
                email.Dispose();
                iResult = 1;
            }
            catch (Exception e)
            {
                iResult = -1;
            }
            return iResult;
        }

        #endregion Private

        #endregion Methods

    }

}

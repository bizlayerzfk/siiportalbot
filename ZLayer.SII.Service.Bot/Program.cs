﻿using System.ServiceProcess;

namespace ZLayer.SII.Service.Bot
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] prmArguments)
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Process()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
